#ifndef __RL_CONDIG_H__
#define __RL_CONDIG_H__

#include <string>
#include <vector>

namespace rlcpp
{
using Int  = int;
using Real = float;

using std::string;
using Veci = std::vector<Int>;
using Vecf = std::vector<Real>;
}  // namespace rlcpp

#endif  // !__RL_CONDIG_H__